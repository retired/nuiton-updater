package org.nuiton.updater;

/*
 * #%L
 * Nuiton Application Updater
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.Map;

/**
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.7
 */
public class ApplicationUpdaterTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ApplicationUpdaterTest.class);

    static private class Callback implements ApplicationUpdaterCallback {

        public Map<String, ApplicationInfo> updateToDo(Map<String, ApplicationInfo> appToUpdate) {
            log.info("Application to update\n" + appToUpdate);
            for (ApplicationInfo info : appToUpdate.values()) {
                info.login = "ApplicationUpdate";
                info.password = new char[]{'p', 'a', 's', 's', 'w', 'o', 'r', 'd'};
            }
            return appToUpdate;
        }

        @Override
        public void startUpdate(ApplicationInfo info) {
            log.info("Start to update: " + info);
        }

        public void updateDone(Map<String, ApplicationInfo> appToUpdate, Map<String, Exception> appUpdateError) {
            for (Map.Entry<String, Exception> e : appUpdateError.entrySet()) {
                log.info(String.format("Error during update for application '%s'", e.getKey()), e.getValue());
            }
            Assert.assertTrue("Error: " + appUpdateError, appUpdateError.isEmpty());
        }

        public void aborted(String propertiesURL, Exception eee) {
            log.info(String.format("Update aborted for url '%s'", propertiesURL), eee);
            Assert.assertTrue(false);
        }

    }

    @Test
    public void testUpdate() throws Exception {
        ApplicationUpdater up = new ApplicationUpdater();
        String url = "file:src/test/resources/ApplicationUpdaterTest.properties";
        File current = new File("src/test/resources/ApplicationUpdater");
        File dest = new File("target/test/ApplicationUpdater/NEW");
        up.update(url, current, dest, false, new Callback());
    }

    @Test
    public void testUpdateNetwork() throws Exception {
        ApplicationUpdater up = new ApplicationUpdater();
        String url = "http://gitweb.nuiton.org/?p=nuiton-updater.git;a=blob_plain;f=src/test/resources/ApplicationUpdaterNetworkTest.properties";
        File current = new File("src/test/resources/ApplicationUpdater");
        File dest = new File("target/test/ApplicationUpdater/NEWNETWORK");
        up.update(url, current, dest, false, new Callback());
    }

    @Test
    public void testUpdateNetworkAuth() throws Exception {
        ApplicationUpdater up = new ApplicationUpdater();
        String url = "http://gitweb.nuiton.org/?p=nuiton-updater.git;a=blob_plain;f=src/test/resources/ApplicationUpdaterNetworkAuthTest.properties";
        File current = new File("src/test/resources/ApplicationUpdater");
        File dest = new File("target/test/ApplicationUpdater/NEWNETWORKAUTH");
        up.update(url, current, dest, false, new Callback());
    }
}
