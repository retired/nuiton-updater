package org.nuiton.updater;

/*
 * #%L
 * Nuiton Application Updater
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.Serializable;

/**
 * TODO
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.7
 */
public class ApplicationInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    public String name;

    public String oldVersion;

    public String newVersion;

    public String url;

    public boolean needAuthentication;

    public String login;

    public char[] password;

    public File destDir;

    public ApplicationInfo(String name,
                           String oldVersion,
                           String newVersion,
                           String url,
                           File destDir,
                           boolean needAuthentication) {
        this.name = name;
        this.oldVersion = oldVersion;
        this.newVersion = newVersion;
        this.url = url;
        this.needAuthentication = needAuthentication;
        this.destDir = destDir;
    }

    public void setAuthentication(String login, char[] password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public String toString() {
        String result = String.format(
                "App: %s, oldVersion: %s, newVersion: %s, url: %s, destDir:%s",
                name, oldVersion, newVersion, url, destDir);
        return result;
    }

}
