package org.nuiton.updater;

/*
 * #%L
 * Nuiton Application Updater
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;

/**
 * Permet d'interagir avec ApplicationUpdater.
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.7
 */
public interface ApplicationUpdaterCallback {
    /**
     * Appeler avant la recuperation des nouvelles versions.
     * <p>
     * Permet de modifier le repertoire destination ou l'url du zip de
     * l'application pour une application/version
     * particuliere ou d'annuler la mise a jour en le supprimant de la map
     * qui sera retourne.</p>
     * <h3>Authentification</h3>
     * Si {@link ApplicationInfo#needAuthentication} est vrai, il faut que
     * les valeurs {@link ApplicationInfo#login} et {@link ApplicationInfo#password}
     * soient renseignees. Si elle ne le sont pas la recuperation de la
     * ressource echouera. Pour des raisons de securite vous pouvez souhaiter
     * mettre le mot de passe sous une forme encrypte. Dans ce cas il doit
     * etre encadrer par '{' et '}'.
     *
     * Pour encrypter le mot de passe vous devez utiliser:
     * <pre>
     * java -cp commons-vfs-2.0.jar org.apache.commons.vfs2.util.EncryptUtil encrypt mypassword
     * </pre>
     *
     * @param appToUpdate liste des applications a mettre a jour
     * @return {@code null} or empty map if we don't want update, otherwize list of app to update
     */
    Map<String, ApplicationInfo> updateToDo(Map<String, ApplicationInfo> appToUpdate);

    /**
     * Appeler au démarrage d'une mise à jour.
     *
     * @param info application à mettre à jour
     * @since 2.7
     */
    void startUpdate(ApplicationInfo info);

    /**
     * Appeler une fois qu'une mise a jour a parfaitement fonctionné.
     *
     * @param appToUpdate    le dictionnaire des applications mises à jour
     * @param appUpdateError le dictionnaires des erreurs rencontrées lors des mises à jour
     */
    void updateDone(Map<String, ApplicationInfo> appToUpdate,
                    Map<String, Exception> appUpdateError);

    /**
     * Called when exception occur during process initialization
     *
     * @param propertiesURL url use to download properties release information
     * @param eee           exception throw during process
     */
    void aborted(String propertiesURL, Exception eee);
}
