package org.nuiton.updater;

/*
 * #%L
 * Nuiton Application Updater
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.vfs2.FileSystemOptions;
import org.nuiton.config.ApplicationConfig;

import java.io.File;
import java.util.Map;

/**
 * Get version of updates.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public class ApplicationUpdaterActionGetVersions extends AbstractApplicationUpdaterAction {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ApplicationUpdaterActionGetVersions.class);

    protected Map<String, ApplicationInfo> updates;

    public ApplicationUpdaterActionGetVersions(ApplicationConfig config,
                                               String vfsPropertiesUrl,
                                               File currentDir) {
        super(config, vfsPropertiesUrl, currentDir);
    }

    public Map<String, ApplicationInfo> getUpdates() {
        return updates;
    }

    @Override
    public void run() {
        try {
            FileSystemOptions vfsConfig = getVFSConfig(config);
            ApplicationConfig releaseConfig = getUpdaterConfig(vfsConfig, vfsPropertiesUrl);

            Map<String, ApplicationInfo> appToUpdate =
                    getVersions(releaseConfig, true, null);

            updates = Maps.newTreeMap();
            updates.putAll(appToUpdate);
        } catch (Exception eee) {
            log.warn("Can't update");
            log.info("Application update aborted because: ", eee);
        }
    }
}
