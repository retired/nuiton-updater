package org.nuiton.updater;

/*
 * #%L
 * Nuiton Application Updater
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * Exception leve lorsque l'archive d'une mise à jour ne respecte pas le bon
 * format (à savoir contenir un uniquement répertoire).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.11
 */
public class UpdateInvalidArchiveLayoutException extends Exception {

    private static final long serialVersionUID = 1L;


    protected final ApplicationInfo info;

    protected final File archiveFile;

    public UpdateInvalidArchiveLayoutException(ApplicationInfo info, File archiveFile) {
        this.info = info;
        this.archiveFile = archiveFile;
    }

    public ApplicationInfo getInfo() {
        return info;
    }

    public File getArchiveFile() {
        return archiveFile;
    }
}
