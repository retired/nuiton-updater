<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.nuiton</groupId>
    <artifactId>nuitonpom</artifactId>
    <version>10.2</version>
  </parent>

  <artifactId>nuiton-updater</artifactId>
  <version>3.0-SNAPSHOT</version>

  <name>Nuiton Application Updater</name>
  <description>Simple Application updater</description>
  <url>https://doc.nuiton.org/nuiton-updater</url>
  <inceptionYear>2013</inceptionYear>

  <developers>

    <developer>
      <name>Benjamin Poussin</name>
      <id>bpoussin</id>
      <email>poussin@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://www.codelutin.com/</organizationUrl>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>developer</role>
      </roles>
    </developer>

    <developer>
      <name>Tony Chemit</name>
      <id>tchemit</id>
      <email>chemit@codelutin.com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://www.codelutin.com/</organizationUrl>
      <timezone>Europe/Paris</timezone>
      <roles>
        <role>developer</role>
      </roles>
    </developer>

  </developers>

  <scm>
    <connection>scm:git:git@gitlab.nuiton.org:nuiton/nuiton-updater.git</connection>
    <developerConnection>scm:git:git@gitlab.nuiton.org:nuiton/nuiton-updater.git</developerConnection>
    <url>https://gitlab.nuiton.org/nuiton/nuiton-updater</url>
  </scm>

  <properties>

    <!-- redmine project id -->
    <projectId>nuiton-updater</projectId>
    <ciViewId>nuiton-updater</ciViewId>
    <javaVersion>1.6</javaVersion>
    <signatureArtifactId>java16</signatureArtifactId>
    <signatureVersion>1.1</signatureVersion>

    <!-- extra files to include in release -->
    <redmine.releaseFiles>${redmine.libReleaseFiles}</redmine.releaseFiles>

    <!-- Post Release configuration -->
    <skipPostRelease>false</skipPostRelease>
  </properties>

  <dependencies>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-utils</artifactId>
      <version>3.0-rc-7</version>
      <exclusions>
        <exclusion>
          <groupId>commons-beanutils</groupId>
          <artifactId>commons-beanutils</artifactId>
        </exclusion>
        <exclusion>
          <groupId>commons-primitives</groupId>
          <artifactId>commons-primitives</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.nuiton.i18n</groupId>
          <artifactId>nuiton-i18n</artifactId>
        </exclusion>
      </exclusions>
    </dependency>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-config</artifactId>
      <version>3.0-rc-1</version>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-vfs2</artifactId>
      <version>2.0</version>
      <exclusions>
        <exclusion>
          <groupId>org.apache.maven.scm</groupId>
          <artifactId>maven-scm-api</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.apache.maven.scm</groupId>
          <artifactId>maven-scm-provider-svnexe</artifactId>
        </exclusion>
      </exclusions>
    </dependency>

    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
      <version>18.0</version>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>3.3.2</version>
    </dependency>

    <dependency>
      <groupId>commons-logging</groupId>
      <artifactId>commons-logging</artifactId>
      <version>1.2</version>
    </dependency>

    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
      <version>2.4</version>
    </dependency>

    <dependency>
      <groupId>commons-httpclient</groupId>
      <artifactId>commons-httpclient</artifactId>
      <version>3.1</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <version>1.2.17</version>
      <scope>provided</scope>
    </dependency>

  </dependencies>

  <profiles>

    <profile>
      <id>reporting</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <reporting>
        <plugins>

          <plugin>
            <artifactId>maven-project-info-reports-plugin</artifactId>
            <version>${projectInfoReportsPluginVersion}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>index</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>

        </plugins>
      </reporting>

    </profile>

    <!-- create assemblies at release time -->
    <profile>
      <id>assembly-profile</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>
      <build>
        <defaultGoal>package</defaultGoal>
        <plugins>

          <!-- launch in a release the assembly automaticly -->
          <plugin>
            <artifactId>maven-assembly-plugin</artifactId>
            <executions>
              <execution>
                <id>create-assemblies</id>
                <phase>package</phase>
                <goals>
                  <goal>single</goal>
                </goals>
              </execution>
            </executions>
            <configuration>
              <attach>false</attach>
              <descriptorRefs>
                <descriptorRef>deps</descriptorRef>
                <descriptorRef>full</descriptorRef>
              </descriptorRefs>
            </configuration>
          </plugin>

        </plugins>

      </build>
    </profile>

  </profiles>
</project>
